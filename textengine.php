<?php
/**
 * Plugin Name: 	Text Engine
 * Plugin URI: 		https://stephengreer.me
 * Description:  	Send text messages with WordPress
 * Version:      	0.1.0
 * Author:       	Stephen Greer
 * Author URI:   	https://stephengreer.me
 * License:      	GPLv2
 * License URI:  	https://www.gnu.org/licenses/gpl-2.0.html
**/

// avoid direct calls to this file, because now WP core and framework has been used
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
// Create plugin instance on plugins_loaded action to maximize flexibility of wp hooks and filters system.
include_once 'vendor/autoload.php';
include_once 'app/class-init.php';
TextEngine\Init::run( __FILE__ );

if ( ! function_exists('write_log')) {
  function write_log ( $log )  {
     if ( is_array( $log ) || is_object( $log ) ) {
        error_log( print_r( $log, true ) );
     } else {
        error_log( $log );
     }
  }
}
