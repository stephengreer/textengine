<?php

namespace TextEngine\Includes\Form;

class Form {

  /**
   * Fields added to form
   *
   * @var array
   */
  private $fields = [];

  /**
   * Markup for the beginning of the form
   *
   * @var string
   */
  private $form_beginning;

  /**
   * Markup for the end of the form
   *
   * @var string
   */
  private $form_end;

  public function __construct( $form_id ) {

    $this->create_form( $form_id );

  }

  /**
   * Create <form> markup
   *
   * @param string $id
   */
  private function create_form( $form_id ) {
    $this->form_beginning = '<form class="te-form" id="' . esc_html( $form_id ) . '">';
    $this->form_end       = '</form>';
  }

  /**
   * Adds field to form
   *
   * @param array $args
   * @return void
   */
  public function add_field( $args = '' ) {

    if ( empty( $args ) ) {
      $args = array();
    }

    $defaults = [
      'type'           => 'text',
      'id'             => 'my_field',
      'label'          => 'My Field',
      'placeholder'    => '',
      'value'          => '',
      'name'           => '',
      'required'       => false,
      'all_option'     => false,
      'options'        => '',
      'allow_multiple' => false,
      'hidden'         => false,
      'accept'         => '',
    ];

    $args = array_merge( $defaults, $args );

    // Set variables if submit
    if ( $args['type'] === 'submit' ):
      $label   = '';
      $element =  '<input type="submit" class="button button-primary" value="' . esc_html( $args['value'] ) . '">';

    // Set variables if select
    elseif ( $args['type'] === 'select' ):

      $label   =  '<label for="' . esc_html( $args['id'] ) . '">' .
                    esc_html( $args['label'] ) . ( $args['required'] === true ? ' *' : '' ) .
                  '</label>';

      if ( $args['allow_multiple'] ):
        $element =  '<select
                      multiple
                      id="' . esc_html( $args['id'] ) . '"
                      ' . ( $args['required'] === true ? 'required' : '' ) . '
                    >';
      else:
        $element =  '<select
                      id="' . esc_html( $args['id'] ) . '"
                      ' . ( $args['required'] === true ? 'required' : '' ) . '
                    >';
      endif;

      if ( ! empty( $args['all_option'] ) ):
        $element .= '<option value="all">' . __( 'All', 'textengine' ) . '</option>';
      endif;

      if ( ! empty( $args['options'] ) ) :
        foreach ( $args['options'] as $option ):
          $element .= '<option
                        value="' . esc_html( $option['value'] ) . '"
                        ' . ( isset( $option['selected'] ) ? 'selected' : '' ) . '
                      >' .
                        esc_html( $option['label'] ) .
                      '</option>';
        endforeach;
      endif;

      $element .= '</select>';

    elseif ( $args['type'] === 'textarea' ):

      $label   =  '<label for="' . esc_html( $args['id'] ) . '">' .
                    esc_html( $args['label'] ) . ( $args['required'] === true ? ' *' : '' ) .
                  '</label>';

      
      $element =  '<textarea
                    id="' . esc_html( $args['id'] ) . '"
                    name="' . $args['name'] . '"
                    placeholder="' . esc_html( $args['placeholder'] ) . '"
                    ' . ( $args['required'] === true ? 'required' : '' ) . '
                  ></textarea>';

    else:
      $label   =  '<label for="' . esc_html( $args['id'] ) . '">' .
                    esc_html( $args['label'] ) . ( $args['required'] === true ? ' *' : '' ) .
                  '</label>';

      $element =  '<input
                    id="' . esc_html( $args['id'] ) . '"
                    type="' . $args['type'] . '"
                    value="' . $args['value'] . '"
                    name="' . $args['name'] . '"
                    accept="' . $args['accept'] . '"
                    placeholder="' . esc_html( $args['placeholder'] ) . '"
                    ' . ( $args['required'] === true ? 'required' : '' ) . '
                  >';
    endif;



    $output = '<div
                id="field-' . esc_html( $args['id'] ) . '"
                class="form-field' . ( $args['hidden'] === true ? ' hidden' : '' ) . '"
              >' .
                $label .
                $element .
              '</div>';

    // Add field to $fields array
    $this->fields[] = $output;
  }

  /**
   * Renders form
   *
   * @param boolean $echo Whether the form should be echoed
   * @return void
   */
  public function render( $echo = true ) {

    $output = '';

    $output .= $this->form_beginning;

    foreach ( $this->fields as $field ) {
      $output .= $field;
    }

    $output .= $this->form_end;

    if ( $echo === true ):
      echo $output;
    else :
      return $output;
    endif;
  }
}