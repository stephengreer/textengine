<?php 

namespace TextEngine\Includes;

use TextEngine\Includes\Message;
use TextEngine\Includes\Contact;
use TextEngine\Includes\Import\Import_Contacts;

/**
 * Class to handles AJAX requests
 */
class Ajax {

  public function __construct() {

    add_action( 'wp_ajax_te_send_message', array( $this, 'send_message' ) );
    add_action( 'wp_ajax_te_add_contact', array( $this, 'add_contact' ) );
    add_action( 'wp_ajax_te_edit_contact', array( $this, 'edit_contact' ) );
    add_action( 'wp_ajax_te_edit_contact_form', array( $this, 'edit_contact_form' ) );
    add_action( 'wp_ajax_te_filter_contacts', array( $this, 'filter_contacts' ) );
    add_action( 'wp_ajax_te_paginate_contacts', array( $this, 'paginate_contacts' ) );
    add_action( 'wp_ajax_te_import_contacts', array( $this, 'import_contacts' ) );
    add_action( 'wp_ajax_te_send_message', array( $this, 'send_message' ) );

  }

  /**
   * AJAX method used to send text message
   *
   * @return string Success message
   */
  public function send_message() {

    $body = esc_html( $_POST['message'] );
    $list = esc_html( $_POST['list'] );

    $message = new Message();
    $message->send( $list, $body );

    echo __( 'Your message has been sent successfully', 'textengine' );

    die();
  }

  /**
   * AJAX method used to add contact
   *
   * @return string Success mesage
   */
  public function add_contact() {
  
    $args = [
      'first_name' => esc_html( $_POST['first_name'] ),
      'last_name'  => esc_html( $_POST['last_name'] ),
      'phone'      => esc_html( $_POST['phone'] ),
      'email'      => esc_html( $_POST['email'] ),
      'lists'      => $_POST['lists'],
    ];

    $contact = new Contact();

    $notice = $contact->add_contact( $args );
    $list_contacts = $contact->list_contacts();

    $output = [
      'notice' => $notice,
      'list_contacts' => $list_contacts,
    ];

    echo json_encode( $output );

    die();
  }

  /**
   * AJAX method used to edit contact
   *
   * @return string Success mesage
   */
  public function edit_contact() {
  
    $args = [
      'id'         => esc_html( $_POST['id'] ),
      'first_name' => esc_html( $_POST['first_name'] ),
      'last_name'  => esc_html( $_POST['last_name'] ),
      'phone'      => esc_html( $_POST['phone'] ),
      'email'      => esc_html( $_POST['email'] ),
      'lists'      => $_POST['lists'],
    ];

    $contact = new Contact();

    $notice = $contact->edit_contact( $args );
    $list_contacts = $contact->list_contacts();

    $output = [
      'notice' => $notice,
      'list_contacts' => $list_contacts,
    ];

    echo json_encode( $output );

    die();
  }

  /**
   * AJAX method used to create edit contact
   *
   * @return string Edit form
   */
  public function edit_contact_form() {
    
    $id = esc_html( $_POST['id'] );

    $contact = new Contact();
    $output = $contact->edit_contact_form( $id );

    echo $output;

    die();
  }

  /**
   * AJAX method used filter contacts
   *
   * @return string
   */
  public function filter_contacts() {

    $filters = [
      'sort_by' => esc_html( $_POST['sort'] ),
      'list' => esc_html( $_POST['list'] ),
    ];

    $contact = new Contact();
    $list_contacts = $contact->list_contacts( $filters );

    echo $list_contacts;

    die();
  }

  /**
   * AJAX method used for paginating contacts
   *
   * @return string
   */
  public function paginate_contacts() {

    $filters = [
      'sort_by' => esc_html( $_POST['sort'] ),
      'list' => esc_html( $_POST['list'] ),
      'page' => esc_html( $_POST['page'] ),
    ];

    $contact = new Contact();
    $list_contacts = $contact->list_contacts( $filters );

    echo $list_contacts;

    die();
  }

  /**
   * AJAX method importing contacts from CSV
   *
   * @return string
   */
  public function import_contacts() {
    $file_id = esc_html( $_POST['file_id'] );
    $import_contacts = new Import_Contacts();

    echo $import_contacts->import( $file_id );
  }

  /**
   * SVG loader
   * 
   * @return string
   */
  public function loader() {
    $svg = '<svg width="120" height="30" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="#333">
                <circle cx="15" cy="15" r="15">
                    <animate attributeName="r" from="15" to="15"
                            begin="0s" dur="0.8s"
                            values="15;9;15" calcMode="linear"
                            repeatCount="indefinite" />
                    <animate attributeName="fill-opacity" from="1" to="1"
                            begin="0s" dur="0.8s"
                            values="1;.5;1" calcMode="linear"
                            repeatCount="indefinite" />
                </circle>
                <circle cx="60" cy="15" r="9" fill-opacity="0.3">
                    <animate attributeName="r" from="9" to="9"
                            begin="0s" dur="0.8s"
                            values="9;15;9" calcMode="linear"
                            repeatCount="indefinite" />
                    <animate attributeName="fill-opacity" from="0.5" to="0.5"
                            begin="0s" dur="0.8s"
                            values=".5;1;.5" calcMode="linear"
                            repeatCount="indefinite" />
                </circle>
                <circle cx="105" cy="15" r="15">
                    <animate attributeName="r" from="15" to="15"
                            begin="0s" dur="0.8s"
                            values="15;9;15" calcMode="linear"
                            repeatCount="indefinite" />
                    <animate attributeName="fill-opacity" from="1" to="1"
                            begin="0s" dur="0.8s"
                            values="1;.5;1" calcMode="linear"
                            repeatCount="indefinite" />
                </circle>
            </svg>';

    return '<div class="te-loader">' . $svg . '</div>';
  }
}