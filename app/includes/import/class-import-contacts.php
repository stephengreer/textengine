<?php 

namespace TextEngine\Includes\Import;

use TextEngine\Includes\Form\Form;
use TextEngine\Includes\Contact;

/**
 * Class to handle importing contacts from spreadsheet
 */
class Import_Contacts {

  /**
   * Create form for importing contacts
   *
   * @return string import form
   */
  public function upload_form() {
    $form = new Form( 'importContacts' );

    $form->add_field( [
      'type'        => 'file',
      'id'          => 'file_upload',
      'label'       => 'File Upload',
      'name'        => 'async-upload',
      'required'    => true,
      'accept'      => '.csv',
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'file_id',
      'label'       => 'File ID',
      'hidden'      => true,
    ] );

    $form->add_field( [
      'type'        => 'submit',
      'id'          => 'submit',
      'value'       => 'Import Contacts',
    ] );

    return $form->render( false );
  }

  /**
   * Import contacts from CSV file
   *
   * @param integer $file_id
   */
  public function import( $file_id ) {

    $contact_obj = new Contact();

    // Parse CSV to array
    $csv_url = wp_get_attachment_url( $file_id );
    $contacts = array_map( 'str_getcsv', file( $csv_url ) );
    array_walk( $contacts, function(&$a) use ( $contacts ) {
      $a = array_combine( $contacts[0], $a );
    });
    array_shift( $contacts );

    foreach ( $contacts as $contact ):

      $list = get_term_by( 'name', $contact['list'], 'contact_list' );
      $list_id = (int) $list->term_id;

      $lists = [
        $list_id,
      ];

      $args = [
        'first_name'  => sanitize_text_field( $contact['first_name'] ),
        'last_name'   => sanitize_text_field( $contact['last_name'] ),
        'phone'       => sanitize_text_field( $contact['phone'] ),
        'email'       => sanitize_text_field( $contact['email'] ),
        'lists'       => $lists,
      ];

      $contact_obj->add_contact( $args );

    endforeach;

    // Remove Upload
    wp_delete_attachment( $file_id, true );
  }
}