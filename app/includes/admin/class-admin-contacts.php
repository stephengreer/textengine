<?php 

namespace TextEngine\Includes\Admin;
use TextEngine\Includes\Setup;

/**
 * Class that creates admin pages
 */
class Admin_Contacts extends Admin_Page {

  public function markup() {
    $setup = new Setup();

    echo '<h1>' . __( 'Manage Contacts', 'textengine' ) . '</h1>';
    echo $setup->add_contact_shortcode();
  }
}