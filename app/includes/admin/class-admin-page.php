<?php 

namespace TextEngine\Includes\Admin;

/**
 * Class that creates admin pages
 */
class Admin_Page {

  public function markup() {
    echo '<h1>' . __( 'Welcome to TextEngine', 'textengine' ) . '</h1>';
  }
}