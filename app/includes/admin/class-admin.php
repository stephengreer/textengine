<?php 

namespace TextEngine\Includes\Admin;
use TextEngine\Includes\Admin\Admin_Page;
use TextEngine\Includes\Admin\Admin_Settings;

/**
 * Class that creates admin pages
 */
class Admin {

  public function __construct() {
    $settings = new Admin_Settings();

    add_action( 'admin_menu', array( $this, 'admin_pages' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );

    add_action( 'wp_ajax_te_set_webhook', array( $settings, 'set_webhook' ) );
    add_action( 'wp_ajax_te_save_settings', array( $settings, 'save_settings' ) );
  }

  public function admin_pages() {
    $message = new Admin_Message();
    $contacts = new Admin_Contacts();
    $settings = new Admin_Settings();

    add_menu_page( 'TextEngine', 'TextEngine', 'manage_options', 'textengine.php', array( $contacts, 'markup' ), 'dashicons-tickets', 6  );
    add_submenu_page( 'textengine.php', 'Send Message', 'Send Message', 'manage_options', 'textengine-send-message.php', array( $message, 'markup' ) ); 
    add_submenu_page( 'textengine.php', 'Manage Contacts', 'Manage Contacts', 'manage_options', 'textengine-manage-contacts.php', array( $contacts, 'markup' ) ); 
    add_submenu_page( 'textengine.php', 'Settings', 'Settings', 'manage_options', 'textengine-settings.php', array( $settings, 'markup' ) ); 
  }

  public function scripts() {
    wp_enqueue_style( 'selectWoo', TEXTENGINE_PLUGIN_URL . 'assets/dist/styles/selectWoo.css', array(), TEXTENGINE_VERSION, 'all' );
    wp_enqueue_script( 'selectWoo', TEXTENGINE_PLUGIN_URL . 'assets/dist/scripts/selectWoo.js', array(), TEXTENGINE_VERSION, true );

    wp_enqueue_style( 'te-admin-basic', TEXTENGINE_PLUGIN_URL . 'assets/dist/styles/admin-basic.css', array(), TEXTENGINE_VERSION, 'all' );
    wp_enqueue_script( 'te-admin', TEXTENGINE_PLUGIN_URL . 'assets/dist/scripts/admin.js', array(), TEXTENGINE_VERSION, true );
  }
}