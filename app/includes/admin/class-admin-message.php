<?php 

namespace TextEngine\Includes\Admin;
use TextEngine\Includes\Setup;

/**
 * Class that creates admin pages
 */
class Admin_Message extends Admin_Page {

  public function markup() {
    $setup = new Setup();

    echo '<h1>' . __( 'Send Message', 'textengine' ) . '</h1>';
    echo $setup->send_message_shortcode();
  }
}