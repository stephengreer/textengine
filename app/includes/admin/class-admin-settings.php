<?php 

namespace TextEngine\Includes\Admin;
use TextEngine\Includes\Setup;
use TextEngine\Includes\Form\Form;

/**
 * Class that creates admin pages
 */
class Admin_Settings extends Admin_Page {

  public function markup() {
    echo '<h1>' . __( 'Settings', 'textengine' ) . '</h1>';

    echo $this->webhook_key();

    echo $this->settings_form();
  }

  public function webhook_key() {
    $key = get_option( 'te_webhook_key' );
    $webhook_url = site_url( '/' ) . '?webhook-listener=twilio&id=' . $key;

    $output = '<div class="te-settings-webhook">';

    $output .= '<button class="button button-primary">' . __( 'Generate Webhook Key', 'textengine' ) . '</button>';
    $output .= '<label for="webhook">Webhook URL</label>';
    $output .= '<input type="text" id="webhook" value="' . $webhook_url . '" disabled>';

    $output .= '</div>';

    return $output;
  }

  public function set_webhook() {
    $key = uniqid();
    update_option( 'te_webhook_key', $key );

    $new_key = get_option( 'te_webhook_key' );
    $webhook_url = site_url( '/' ) . '?webhook-listener=twilio&id=' . $key;

    echo $webhook_url;

    die();
  }

  public function settings_form() {

    $twilio_sid = get_option( 'te_twilio_sid' );
    $twilio_token = get_option( 'te_twilio_token' );
    $from_number = get_option( 'te_from_number' );

    $form = new Form( 'settings' );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'twilio_sid',
      'label'       => 'Twilio SID',
      'required'    => true,
      'value'       => $twilio_sid,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'twilio_token',
      'label'       => 'Twilio Token',
      'required'    => true,
      'value'       => $twilio_token,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'from_number',
      'label'       => 'From Number',
      'required'    => true,
      'value'       => $from_number,
    ] );

    $form->add_field( [
      'type'        => 'submit',
      'id'          => 'submit',
      'value'       => 'Save',
    ] );

    return $form->render( false );
  }

  public function save_settings() {

    $twilio_sid = esc_html( $_POST['twilio_sid'] );
    $twilio_token = esc_html( $_POST['twilio_token'] );
    $from_number = esc_html( $_POST['from_number'] );
    
    update_option( 'te_twilio_sid', $twilio_sid );
    update_option( 'te_twilio_token', $twilio_token );
    update_option( 'te_from_number', $from_number );

    echo '<div class="notice notice-success is-dismissible"> 
      <p><strong>' . __( 'Settings saved.', 'textengine' ) . '</strong></p>
      <button type="button" class="notice-dismiss">
        <span class="screen-reader-text">Dismiss this notice.</span>
      </button>
    </div>';

    die();
  }
}