<?php 

namespace TextEngine\Includes;

use Twilio\Rest\Client;
use TextEngine\Includes\Form\Form;
use TextEngine\Includes\Contact;
use WP_Query;
use Twilio\Twiml;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

/**
 * Class to handle messages
 */
class Message {

  /**
   * Send text message
   *
   * @param int $list_id
   * @param string $body
   */
  public function send( $list_id, $body ) {

    // Your Account SID and Auth Token from twilio.com/console
    $sid = get_option( 'te_twilio_sid' );
    $token = get_option( 'te_twilio_token' );
    $from_number = get_option( 'te_from_number' );

    $client = new Client( $sid, $token );
    $numbers = $this->list_numbers( $list_id );

    foreach ( $numbers as $number ) {
      
      $client->messages->create(
        $number,
        array(
          'from' => $from_number,
          'body' => $body
        )
      );
    }
  }

  public function send_form() {
    $form    = new Form( 'sendMessage' );
    $contact = new Contact();

    $form->add_field( [
      'type'        => 'textarea',
      'id'          => 'message',
      'label'       => 'Message',
      'required'    => true,
    ] );

    $form->add_field( [
      'type'           => 'select',
      'id'             => 'list',
      'label'          => 'List',
      'options'        => $contact->list_contact_lists( true ),
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'submit',
      'id'          => 'submit',
      'value'       => 'Send Message',
    ] );

    return $form->render( false );
  }

  /**
   * Get array of phone numbers for list
   * 
   * @param int $list_id
   * 
   * @return array
   */
  public function list_numbers( $list_id ) {
    $args = array(
      'post_type'              => [ 'contact' ],
      'tax_query' => [
          [
            'taxonomy' => 'contact_list',
            'field'    => 'id',
            'terms'    => $list_id,
          ],
        ],
    );
  
    // The Query
    $query = new WP_Query( $args );
  
    // The Loop
    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        $numbers[] = get_post_meta( get_the_ID(), 'te_phone', true );
      }
    }
  
    // Restore original Post Data
    wp_reset_postdata();
  
    return $numbers;
  }

  /**
   * Adds webhook that handles messages from Twilio
   */
  public function webhook_listener() {
    $webhook_id = get_option( 'te_webhook_key' );

    if ( isset( $_GET['webhook-listener'] ) && $_GET['webhook-listener'] == 'twilio' ) {
      
      if ( esc_html( $_GET['id'] ) == $webhook_id ) {
        
        $response = new Twiml();
        echo $response;

        if ( ! empty( $_GET['From'] ) && ! empty( $_GET['Body'] ) ) {
          $from = esc_html( $_GET['From'] );
          $message = esc_html( $_GET['Body'] );

          $this->add_message( $from, $message );
        }

      } else {
        echo 'Fail';
      }

      die();
    }
  }

  /**
   * Adds message to DB as post
   *
   * @param string $from
   * @param string $message
   * @return string Success/Failure notice
   */
  public function add_message( $from, $message ) {

    $phone_util = PhoneNumberUtil::getInstance();
    $phone_object = $phone_util->parse( $from, 'US' );
    $phone_valid = $phone_util->isPossibleNumber( $phone_object );
    $phone_formated = $phone_util->format( $phone_object, PhoneNumberFormat::E164 );

    $post = [
      'post_title'  => $phone_formated,
      'post_content' => esc_html( $message ),
      'post_type'   => 'message',
      'post_status' => 'publish',
    ];

    $post_id = wp_insert_post( $post );
    add_post_meta( $post_id, 'te_from', $phone_formated, true );

    return __( 'Contact was added successfully', 'textengine' );
  }

  public function get_messages() {
    $args = [
      'post_type'         => [ 'message' ],
      'posts_per_page'    => '100',
    ];

    // The Query
    $messages = new WP_Query( $args );

    $output = '';

    // The Loop
    if ( $messages->have_posts() ) {
      while ( $messages->have_posts() ) {
        $messages->the_post();
        $from_number = get_post_meta( get_the_ID(), 'te_from', true );
        $message = get_the_content();
        $contacts = $this->find_contact( $from_number );

        $output .= '<div class="message-item">';

        if ( ! empty( $contacts ) ):
          $output .= '<div class="from">';
          $output .= implode( $contacts, ' or ' );
          $output .= '</div>';
        else:
          $output .= '<div class="from">' . $from_number . '</div>';
        endif;

        $output .= '<div class="message">' . $message . '</div>';

        $output .= '</div>';
      }
    }

    return $output;

    // Restore original Post Data
    wp_reset_postdata();
  }

  /**
   * Check if phone number is attached to a contact
   *
   * @param string $phone_number
   * @return array $contacts
   */
  private function find_contact( $phone_number ) {

    $contacts = [];

    global $wpdb;

    $results = $wpdb->get_results ( "
        SELECT post_id
        FROM {$wpdb->postmeta}
        WHERE meta_key = 'te_phone'
      " );

    foreach ( $results as $result ):
      if ( get_post_status( $result->post_id ) === 'publish' ):
        $contacts[] = get_the_title( $result->post_id );
      endif;
    endforeach;

    return $contacts;
  }

  /**
   * Create message post type
   */
  public function post_type() {
    $labels = array(
      'name'                  => 'Messages',
      'singular_name'         => 'Message',
      'menu_name'             => 'Messages',
      'name_admin_bar'        => 'Message',
      'archives'              => 'Item Archives',
      'attributes'            => 'Item Attributes',
      'parent_item_colon'     => 'Parent Item:',
      'all_items'             => 'All Messages',
      'add_new_item'          => 'Add New Item',
      'add_new'               => 'Add New',
      'new_item'              => 'New Item',
      'edit_item'             => 'Edit Item',
      'update_item'           => 'Update Item',
      'view_item'             => 'View Item',
      'view_items'            => 'View Items',
      'search_items'          => 'Search Item',
      'not_found'             => 'Not found',
      'not_found_in_trash'    => 'Not found in Trash',
      'featured_image'        => 'Featured Image',
      'set_featured_image'    => 'Set featured image',
      'remove_featured_image' => 'Remove featured image',
      'use_featured_image'    => 'Use as featured image',
      'insert_into_item'      => 'Insert into item',
      'uploaded_to_this_item' => 'Uploaded to this item',
      'items_message list'            => 'Items message list',
      'items_message list_navigation' => 'Items message list navigation',
      'filter_items_message list'     => 'Filter items message list',
    );
    $args = array(
      'label'                 => 'Message',
      'description'           => 'Post Type Description',
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,		
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    register_post_type( 'message', $args );
  }
}