<?php 

namespace TextEngine\Includes;

use TextEngine\Includes\Contact;
use TextEngine\Includes\Ajax;
use TextEngine\Includes\Import\Import_Contacts;

/**
 * Class to sets up the plugin
 */
class Setup {

  public function __construct() {

    $contact = new Contact;
    $message = new Message;

    add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );

    // Create contact post type on init
    add_action( 'init', array( $contact, 'post_type' ) );

    // Create contact list taxonomy type on init
    add_action( 'init', array( $contact, 'contact_list_taxonomy' ) );

    // Create message post type on init
    add_action( 'init', array( $message, 'post_type' ) );

    // Shortcodes
    if ( is_user_logged_in() ):
      add_shortcode( 'te_contacts', array( $this, 'add_contact_shortcode' ) );
      add_shortcode( 'te_import_contacts', array( $this, 'import_contacts_shortcode' ) );
      add_shortcode( 'te_send_message', array( $this, 'send_message_shortcode' ) );
    else:
      add_shortcode( 'te_contacts', array( $this, 'login_form' ) );
      add_shortcode( 'te_import_contacts', array( $this, 'login_form' ) );
      add_shortcode( 'te_send_message', array( $this, 'login_form' ) );
    endif;

    add_action( 'init', array( $message, 'webhook_listener' ) );
  }

  /**
   * Build te_contacts shortcode
   *
   * @return void
   */
  public function add_contact_shortcode() {
    $contact = new Contact();
    $ajax = new Ajax();

    echo '<div class="te-contacts">';

    echo '<h2>' . __( 'Add Contact', 'textengine' ) . '</h2>';
    
    echo $contact->add_contact_form();

    echo '<h2>' . __( 'All Contacts', 'textengine' ) . '</h2>';

    echo '<div class="contact-filters">';
    echo $contact->contact_filters();
    echo '</div>';

    echo '<div class="contact-list">';

    echo '<div class="all-contacts">';
    echo $contact->list_contacts();
    echo '</div>';

    echo '<div class="edit-contacts"></div>';

    echo '</div>';

    echo $ajax->loader();

    echo '</div>';
  }

  /**
   * Build import contacts shortcode
   */
  public function import_contacts_shortcode() {
    $import_contacts = new Import_Contacts();
    $ajax = new Ajax();

    echo '<div class="te-import-contacts">';

    echo '<h2>' . __( 'Import Contacts', 'textengine' ) . '</h2>';
    
    echo $import_contacts->upload_form();

    echo $ajax->loader();

    echo '</div>';
  }

  /**
   * Build send message shortcode
   */
  public function send_message_shortcode() {
    $message = new Message();
    $ajax = new Ajax();

    echo '<div class="te-send-message">';

    echo '<h2>' . __( 'Send Message', 'textengine' ) . '</h2>';
    echo $message->send_form();
    echo $ajax->loader();

    echo '</div>';

    echo '<div class="te-received-messages">';

    echo '<h2>' . __( 'Received Messages', 'textengine' ) . '</h2>';
    echo $message->get_messages();

    echo '</div>';
  }

  /**
   * Scripts to be enqueued
   */
  public function scripts() {

    wp_enqueue_style( 'selectWoo', TEXTENGINE_PLUGIN_URL . 'assets/dist/styles/selectWoo.css', array(), TEXTENGINE_VERSION, 'all' );
    wp_enqueue_script( 'selectWoo', TEXTENGINE_PLUGIN_URL . 'assets/dist/scripts/selectWoo.js', array(), TEXTENGINE_VERSION, true );

    wp_enqueue_style( 'te-frontend-basic', TEXTENGINE_PLUGIN_URL . 'assets/dist/styles/frontend-basic.css', array(), TEXTENGINE_VERSION, 'all' );
    wp_enqueue_script( 'te-frontend', TEXTENGINE_PLUGIN_URL . 'assets/dist/scripts/frontend.js', array(), TEXTENGINE_VERSION, true );

    $data = array(
      'admin_ajax'   => admin_url( 'admin-ajax.php' ),
      'async_upload' => admin_url( 'async-upload.php' ),
      'nonce'        => wp_create_nonce( 'media-form' ),
    );

    wp_localize_script( 'te-frontend', 'te_frontend', $data );
  }

  /**
   * Create login form
   *
   * @return string
   */
  public function login_form() {
    $args = array(
      'echo'           => false,
      'remember'			 => false,
      'redirect'       => site_url(),
      'label_username' => __( 'Username' ),
      'label_password' => __( 'Password' ),
      'label_remember' => __( 'Remember Me' ),
      'label_log_in'   => __( 'Log In' )
    );

    $output = '<div class="te-login">' .
                '<h2>Login</h2>' .
                wp_login_form( $args );
              '</div>';

    return $output;
  }
}