<?php 

namespace TextEngine\Includes;

use TextEngine\Includes\Form\Form;
use WP_Query;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

/**
 * Class to handle contacts
 */
class Contact {

  /**
   * Adds contact to DB as post
   *
   * @param array $args
   * @return string Success/Failure notice
   */
  public function add_contact( $args = [] ) {

    $phone_util = PhoneNumberUtil::getInstance();
    $phone_object = $phone_util->parse( $args['phone'], 'US' );
    $phone_valid = $phone_util->isPossibleNumber( $phone_object );
    $phone_formated = $phone_util->format( $phone_object, PhoneNumberFormat::E164 );

    $post = [
      'post_title'  => $args['first_name'] . ' ' . $args['last_name'],
      'post_type'   => 'contact',
      'post_status' => 'publish',
    ];

    if ( $phone_valid ):
      $post_id = wp_insert_post( $post );
      add_post_meta( $post_id, 'te_first_name', $args['first_name'], true );
      add_post_meta( $post_id, 'te_last_name', $args['last_name'], true );
      add_post_meta( $post_id, 'te_phone', $phone_formated, true );
      add_post_meta( $post_id, 'te_email', $args['email'], true );

      // Handle lists taxonomy
      if ( isset( $args['lists'] ) ):

        $existing_terms = [];
        $new_terms = [];
        $new_term_ids = [];

        // Divid terms into existing terms array and new terms array
        foreach( $args['lists'] as $list ):
          if ( is_numeric( $list ) ):
            $existing_terms[] = $list;
          else:
            $new_terms[] = $list;
          endif;
        endforeach;     

        // Add new terms to DB and put their ids in array
        if ( ! empty( $new_terms ) ):
          foreach( $new_terms as $new_term ):
            $new_term = wp_insert_term( $new_term, 'contact_list' );
            $new_term_ids[] = $new_term['term_id'];
          endforeach;
        endif;

        // Combine existing term ids with new term ids
        $all_terms = array_merge( $existing_terms, $new_term_ids );

        wp_set_post_terms( $post_id, $all_terms, 'contact_list' );

      endif;

    endif;

    if ( $phone_valid != true ):
      return __( 'Invalid phone number', 'textengine' );
    elseif ( $post_id === 0 ):
      return __( 'There was an error adding contact', 'textengine' );
    else:
      return __( 'Contact was added successfully', 'textengine' );
    endif;
  }

  /**
   * Create add contact form
   * 
   * @return string Add contact form
   */
  public function add_contact_form() {
    $form = new Form( 'addContact' );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'first_name',
      'label'       => 'First Name',
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'last_name',
      'label'       => 'Last Name',
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'phone',
      'label'       => 'Phone Number',
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'email',
      'id'          => 'email',
      'label'       => 'Email',
    ] );

    $form->add_field( [
      'type'           => 'select',
      'allow_multiple' => true,
      'id'             => 'lists',
      'label'          => 'Lists',
      'options'        => $this->list_contact_lists(),
    ] );

    $form->add_field( [
      'type'        => 'submit',
      'id'          => 'submit',
      'value'       => 'Add Contact',
    ] );

    return $form->render( false );
  }

  /**
   * Create list of contacts
   *
   * @return string
   */
  public function list_contacts( $filters = '' ) {

    if ( empty( $filters ) ) {
      $filters = [];
    }

    $defaults = [
      'sort_by' => 'date',
      'list' => '',
      'page' => 1,
    ];

    $filters = array_merge( $defaults, $filters );

    switch ( $filters['sort_by'] ):
      case 'date':
        $order    = 'DESC';
        $orderby  = 'date';
        $meta_key = 'te_first_name';
        break;

      case 'first_name':
        $order    = 'ASC';
        $orderby  = 'meta_value';
        $meta_key = 'te_first_name';
        break;

      case 'last_name':
        $order    = 'ASC';
        $orderby  = 'meta_value';
        $meta_key = 'te_last_name';
        break;

    endswitch;

    if ( ! empty( $filters['list'] ) && $filters['list'] != 'all' ):
      $tax_query = [
        [
          'taxonomy' => 'contact_list',
          'terms'    => $filters['list'],
        ],
      ];
    else: 
      $tax_query = '';
    endif;

    // WP_Query arguments
    $args = [
      'post_type'         => [ 'contact' ],
      'paged'             => $filters['page'],
      'posts_per_page'    => '50',
      'order'             => $order,
      'orderby'           => $orderby,
      'meta_key'          => $meta_key,
      'tax_query'         => $tax_query,
    ];

    // The Query
    $contacts = new WP_Query( $args );

    $output = '';

    // The Loop
    if ( $contacts->have_posts() ) {
      while ( $contacts->have_posts() ) {
        $contacts->the_post();
        $contact_meta = get_post_meta( get_the_ID() );
        $lists = get_the_terms( get_the_ID(), 'contact_list' );
        $list_names = [];

        if ( ! empty( $lists ) ):
          foreach ( $lists as $list ):
            $list_names[] = $list->name;
          endforeach;
        endif;

        $phone_util = PhoneNumberUtil::getInstance();
        $phone_object = $phone_util->parse( $contact_meta['te_phone'][0], 'US' );
        $phone_formatted = $phone_util->format( $phone_object, PhoneNumberFormat::NATIONAL );

        $output .= '<div class="contact-item" data-id="' . get_the_ID() . '">';

        $output .= '<div class="first-name">';
        if ( isset( $contact_meta['te_first_name'][0] ) ):
          $output .= '<span>' . esc_html( $contact_meta['te_first_name'][0] ) . '</span>';
        endif;
        $output .= '</div>';

        $output .= '<div class="last-name">';
        if ( isset( $contact_meta['te_last_name'][0] ) ):
          $output .= '<span>' . esc_html( $contact_meta['te_last_name'][0] ) . '</span>';
        endif;
        $output .= '</div>';

        $output .= '<div class="phone">';
        if ( isset( $contact_meta['te_phone'][0] ) ):
          $output .= '<span>' . $phone_formatted . '</span>';
        endif;
        $output .= '</div>';

        $output .= '<div class="email">';
        if ( isset( $contact_meta['te_email'][0] ) ):
          $output .= '<span>' . esc_html( $contact_meta['te_email'][0] ) . '</span>';
        endif;
        $output .= '</div>';

        $output .= '<div class="lists">';
        if ( ! empty( $list_names ) ):
          $output .= '<span>' . implode( ', ', $list_names ) . '</span>';
        endif;
        $output .= '</div>';

        $output .= '<div class="actions">';
        $output .= '<button id="edit" class="button button-primary">Edit</button>';
        $output .= '</div>';

        $output .= '</div>';
      }
    } else {
      // no posts found
    }

    // Pagination
    if ( $contacts->max_num_pages > 1):
      $output .= '<div class="te-pagination">';

      for ( $i = 1; $i <= $contacts->max_num_pages ; $i++ ):
        if ( $i == $filters['page'] ):
          $output .= '<a href="#" class="current">' . $i . '</a>';
        else:
          $output .= '<a href="#">' . $i . '</a>';
        endif;
      endfor;

      $output .= '</div>';
    endif;

    return $output;  

    // Restore original Post Data
    wp_reset_postdata();
  }

  /**
   * Create contact filters form
   *
   * @return string filters form
   */
  public function contact_filters() {
    $form = new Form( 'filterContacts' );
    
    $form->add_field( [
      'type'        => 'select',
      'id'          => 'sort',
      'label'       => 'Sort By',
      'options'     => [
                          [
                            'value' => 'date',
                            'label' => 'Date Added',
                          ],
                          [
                            'value' => 'first_name',
                            'label' => 'First Name',
                          ],
                          [
                            'value' => 'last_name',
                            'label' => 'Last Name',
                          ],
                        ],
    ] );

    $form->add_field( [
      'type'        => 'select',
      'id'          => 'list',
      'label'       => 'List',
      'all_option'  => true,
      'options'     => $this->list_contact_lists( true ),
    ] );
    
    return $form->render( false );
  }

  /**
   * Create array of contact list names and ids
   *
   * @return array
   */
  public function list_contact_lists( $hide_empty = false ) {
    $lists = get_terms( [
        'taxonomy'   => 'contact_list',
        'hide_empty' => $hide_empty,
     ] );

     $list_options = [];

     foreach ( $lists as $list ):
       $list_options[] = [
         'value' => $list->term_id,
         'label' => $list->name,
       ];
     endforeach;

     return $list_options;
  }

  /**
   * Create edit contact form
   * 
   * @return string Edit contact form
   */
  public function edit_contact_form( $post_id ) {

    $first_name = get_post_meta( $post_id, 'te_first_name', true );
    $last_name = get_post_meta( $post_id, 'te_last_name', true );
    $phone = get_post_meta( $post_id, 'te_phone', true );
    $email = get_post_meta( $post_id, 'te_email', true );
    $current_lists = wp_get_post_terms( $post_id, 'contact_list' );
    $all_list_options = $this->list_contact_lists();

    // Add selected to current list options
    $i = 0;
    foreach ( $all_list_options as $all_list_option ):
      $selected = '';
      
      foreach ( $current_lists as $current_list ):
        if ( $current_list->term_id === $all_list_option['value'] ):
          $selected = true;
        endif;
      endforeach;

      if ( $selected === true ):
        $all_list_options[$i]['selected'] = true;
      endif;

      $i++;
    endforeach;


    $form = new Form( 'editContact' );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'id',
      'value'       => $post_id,
      'hidden'      => true,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'first_name',
      'label'       => 'First Name',
      'value'       => $first_name,
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'last_name',
      'label'       => 'Last Name',
      'value'       => $last_name,
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'text',
      'id'          => 'phone',
      'label'       => 'Phone Number',
      'value'       => $phone,
      'required'    => true,
    ] );

    $form->add_field( [
      'type'        => 'email',
      'id'          => 'email',
      'label'       => 'Email',
      'value'       => $email,
    ] );

    $form->add_field( [
      'type'           => 'select',
      'allow_multiple' => true,
      'id'             => 'lists',
      'label'          => 'Lists',
      'options'        => $all_list_options,
    ] );

    $form->add_field( [
      'type'        => 'submit',
      'id'          => 'submit',
      'value'       => 'Edit Contact',
    ] );

    $output .= '<h2>' . __( 'Edit Contact', 'textengine' ) . '</h2>';
    $output .= $form->render( false );

    return $output;
  }

  /**
   * Edit contact in DB
   *
   * @param array $args
   * @return string Success/Failure notice
   */
  public function edit_contact( $args = [] ) {

    $phone_util = PhoneNumberUtil::getInstance();
    $phone_object = $phone_util->parse( $args['phone'], 'US' );
    $phone_valid = $phone_util->isPossibleNumber( $phone_object );
    $phone_formated = $phone_util->format( $phone_object, PhoneNumberFormat::E164 );

    $post = [
      'ID'          => $args['id'],
      'post_title'  => $args['first_name'] . ' ' . $args['last_name'],
      'post_type'   => 'contact',
      'post_status' => 'publish',
    ];

    if ( $phone_valid ):
      $post_id = wp_update_post( $post );
      update_post_meta( $post_id, 'te_first_name', $args['first_name'] );
      update_post_meta( $post_id, 'te_last_name', $args['last_name'] );
      update_post_meta( $post_id, 'te_phone', $phone_formated );
      update_post_meta( $post_id, 'te_email', $args['email'] );

      // Handle lists taxonomy
      if ( isset( $args['lists'] ) ):

        $existing_terms = [];
        $new_terms = [];
        $new_term_ids = [];

        // Divid terms into existing terms array and new terms array
        foreach( $args['lists'] as $list ):
          if ( is_numeric( $list ) ):
            $existing_terms[] = $list;
          else:
            $new_terms[] = $list;
          endif;
        endforeach;     

        // Add new terms to DB and put their ids in array
        if ( ! empty( $new_terms ) ):
          foreach( $new_terms as $new_term ):
            $new_term = wp_insert_term( $new_term, 'contact_list' );
            $new_term_ids[] = $new_term['term_id'];
          endforeach;
        endif;

        // Combine existing term ids with new term ids
        $all_terms = array_merge( $existing_terms, $new_term_ids );

        wp_set_post_terms( $post_id, $all_terms, 'contact_list' );

      endif;

    endif;

    if ( $phone_valid != true ):
      return __( 'Invalid phone number', 'textengine' );
    elseif ( $post_id === 0 ):
      return __( 'There was an error editing contact', 'textengine' );
    else:
      return __( 'Contact was edited successfully', 'textengine' );
    endif;
  }

  /**
   * Create contact post type
   */
  public function post_type() {
    $labels = array(
      'name'                  => 'Contacts',
      'singular_name'         => 'Contact',
      'menu_name'             => 'Contacts',
      'name_admin_bar'        => 'Contact',
      'archives'              => 'Item Archives',
      'attributes'            => 'Item Attributes',
      'parent_item_colon'     => 'Parent Item:',
      'all_items'             => 'All Contacts',
      'add_new_item'          => 'Add New Item',
      'add_new'               => 'Add New',
      'new_item'              => 'New Item',
      'edit_item'             => 'Edit Item',
      'update_item'           => 'Update Item',
      'view_item'             => 'View Item',
      'view_items'            => 'View Items',
      'search_items'          => 'Search Item',
      'not_found'             => 'Not found',
      'not_found_in_trash'    => 'Not found in Trash',
      'featured_image'        => 'Featured Image',
      'set_featured_image'    => 'Set featured image',
      'remove_featured_image' => 'Remove featured image',
      'use_featured_image'    => 'Use as featured image',
      'insert_into_item'      => 'Insert into item',
      'uploaded_to_this_item' => 'Uploaded to this item',
      'items_contact list'            => 'Items contact list',
      'items_contact list_navigation' => 'Items contact list navigation',
      'filter_items_contact list'     => 'Filter items contact list',
    );
    $args = array(
      'label'                 => 'Contact',
      'description'           => 'Post Type Description',
      'labels'                => $labels,
      'supports'              => array( 'title' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,		
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    register_post_type( 'contact', $args );
  }

  /**
   * Create contact_list taxonomy
   */
  public function contact_list_taxonomy() {
    $labels = array(
      'name'                       => 'Contact Lists',
      'singular_name'              => 'Contact List',
      'menu_name'                  => 'Contact Lists',
      'all_items'                  => 'All Contact Lists',
      'parent_item'                => 'Parent Contact List',
      'parent_item_colon'          => 'Parent Contact List:',
      'new_item_name'              => 'New Contact List Name',
      'add_new_item'               => 'Add New Contact List',
      'edit_item'                  => 'Edit Contact List',
      'update_item'                => 'Update Contact List',
      'view_item'                  => 'View Item',
      'separate_items_with_commas' => 'Separate contact lists with commas',
      'add_or_remove_items'        => 'Add or remove contact lists',
      'choose_from_most_used'      => 'Choose from the most used contact lists',
      'popular_items'              => 'Popular Items',
      'search_items'               => 'Search contact lists',
      'not_found'                  => 'Not Found',
      'no_terms'                   => 'No items',
      'items_list'                 => 'Items contact list',
      'items_list_navigation'      => 'Items contact list navigation',
    );
    $args = array(
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => true,
    );
    register_taxonomy( 'contact_list', array( 'contact' ), $args );
  }
}