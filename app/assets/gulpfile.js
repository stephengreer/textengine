var gulp = require('gulp');
    plugins = require('gulp-load-plugins')(),
    browserSync = require('browser-sync').create(),
    del = require('del'),
    manifest = require('./source/manifest.json');

var assets = manifest.assets;
var config = manifest.config;

gulp.task('sass', function(){
  return gulp.src(assets.styles)
    .pipe(plugins.plumber({
      errorHandler: plugins.notify.onError("Error: <%= error.message %>")
    }))
    .pipe(plugins.sass()) // Using gulp-sass
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(plugins.cssnano({
      zindex: false
    }))
    .pipe(plugins.size())
    .pipe(gulp.dest('dist/styles'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('scripts', function(){
  return gulp.src(assets.scripts)
    // .pipe(plugins.concat('main.js'))
    .pipe(plugins.minify({
      ext:{
        min:'.js'
      },
      noSource: true
    }))
    .pipe(plugins.jshint())
    .pipe(plugins.plumber({
      errorHandler: plugins.notify.onError("Error: <%= error.message %>")
    }))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('clean', function () {
  return del([
    'dist',
  ]);
});

gulp.task('watch', ['sass', 'scripts'], function() {

  browserSync.init({
    proxy: "http://" + config.devUrl,
    host: config.devUrl,
    files: "*.php"
  });

  gulp.watch('./source/styles/*.scss', ['sass']);
  gulp.watch('./source/styles/**/*.scss', ['sass']);
  gulp.watch('./source/scripts/*.js', ['scripts']);
});

gulp.task('default', ['sass', 'scripts']);