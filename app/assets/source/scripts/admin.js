(function($){

  /**
   * selectWoo Init
   */
  $( document ).ready( function() {
    $( 'form#addContact select#lists' ).selectWoo( {
      tags: true,
      placeholder: 'Select Lists',
    } );

    $( 'form#editContact select#lists' ).selectWoo( {
      tags: true,
      placeholder: 'Select Lists',
    } );

    $( 'form#sendMessage select#list' ).selectWoo( {
      placeholder: 'Select List',
    } );
  } );


  function showLoader() {
    var top = $( window ).scrollTop();
    $( '.te-loader' ).show();
    $( '.te-loader svg' ).css( 'top', top + 'px' );
  }

  function hideLoader() {
    $( '.te-loader' ).hide();
  }

  /**
   * AJAX Add new contact
   * 
   * @param {*} thisObj 
   */
  function addContact( thisObj ) {

    var firstName = thisObj.find( '#first_name' ).val(),
        lastName  = thisObj.find( '#last_name' ).val(),
        phone     = thisObj.find( '#phone' ).val(),
        email     = thisObj.find( '#email' ).val();
        lists     = thisObj.find( '#lists' ).val();

    showLoader();
    $( '.te-notice' ).remove();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      dataType: 'json',
      data: {
        action: 'te_add_contact',
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email,
        lists: lists
      },
      success: function( response ) {
        thisObj.before( '<div class="te-notice">' + response.notice + '</div>' );
        thisObj.trigger( 'reset' );
        thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        $( '.all-contacts' ).html( response.list_contacts );
        hideLoader();
      }
    });
  }

  /**
   * Add contact trigger
   */
  $( document ).on( 'submit', 'form#addContact', function(event) {
    event.preventDefault();
    addContact( $( this ) );    
  });

  /**
   * AJAX Edit contact
   * 
   * @param {*} thisObj 
   */
  function editContact( thisObj ) {

    var id        = thisObj.find( '#id' ).val(),
        firstName = thisObj.find( '#first_name' ).val(),
        lastName  = thisObj.find( '#last_name' ).val(),
        phone     = thisObj.find( '#phone' ).val(),
        email     = thisObj.find( '#email' ).val();
        lists     = thisObj.find( '#lists' ).val();

    showLoader();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      dataType: 'json',
      data: {
        action: 'te_edit_contact',
        id: id,
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email,
        lists: lists
      },
      success: function( response ) {
        // thisObj.before( '<div class="te-notice">' + response.notice + '</div>' );
        // thisObj.trigger( 'reset' );
        // thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        $( '.contact-list' ).removeClass( 'editor-open' );
        $( '.edit-contacts' ).hide();
        $( '.all-contacts' ).html( response.list_contacts );
        hideLoader();
      }
    });
  }

  /**
   * Edit contact trigger
   */
  $( document ).on( 'submit', 'form#editContact', function(event) {
    event.preventDefault();
    editContact( $( this ) ); 
  });

  /**
   * AJAX Open edit contact form
   * 
   * @param {*} thisObj
   */
  function editContactOpen( thisObj ) {

    var contactId = thisObj.parents( '.contact-item' ).data( 'id' );
    
    showLoader();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_edit_contact_form',
        id: contactId
      },
      success: function( response ) {
        $( '.contact-list' ).addClass( 'editor-open' );
        $( '.edit-contacts' ).show();
        $( '.edit-contacts' ).html( response );
        $( '.edit-contacts' ).find( 'select' ).selectWoo();
        hideLoader();
      }
    });
  }

  /**
   * Edit contact open form
   */
  $( document ).on( 'click', '.contact-item button#edit', function(event) {
    event.preventDefault();
    editContactOpen( $( this ) );
  });

  /**
   * AJAX Filter Contacts
   */
  function filterContacts() {

    var sort = $( 'form#filterContacts #sort' ).val(),
        list = $( 'form#filterContacts #list' ).val();

    showLoader();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_filter_contacts',
        sort: sort,
        list: list
      },
      success: function( response ) {
        $( '.contact-list .all-contacts' ).html( response );
        hideLoader();
      }
    });
  }

  // Filter Contacts
  $( document ).on( 'change', 'form#filterContacts select', function(event) {
    event.preventDefault();
    filterContacts();
  });

  /**
   * AJAX Paginate Contacts
   */
  function paginateContacts( page ) {

    var sort = $( 'form#filterContacts #sort' ).val(),
        list = $( 'form#filterContacts #list' ).val();

    showLoader();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_paginate_contacts',
        sort: sort,
        list: list,
        page: page,
      },
      success: function( response ) {
        $( '.contact-list .all-contacts' ).html( response );
        hideLoader();
      }
    });
  }

  // Paginate Contacts
  $( document ).on( 'click', '.contact-list .te-pagination a', function(event) {
    event.preventDefault();
    var page = $( this ).text();
    paginateContacts( page );
  });

  /**
   * AJAX Send Message
   * 
   * @param {*} thisObj 
   */
  function sendMessage( thisObj ) {

    var message = thisObj.find( '#message' ).val(),
        list   = thisObj.find( '#list' ).val();

    showLoader();
    $( '.te-notice' ).remove();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_send_message',
        message: message,
        list: list
      },
      success: function( response ) {
        thisObj.before( '<div class="te-notice">' + response + '</div>' );
        thisObj.trigger( 'reset' );
        thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        hideLoader();
      }
    });
  }

  /**
   * Send message trigger
   */
  $( document ).on( 'submit', 'form#sendMessage', function(event) {
    event.preventDefault();
    sendMessage( $( this ) );    
  });

  /**
   * AJAX Setting - webhook reset
   */
  function settingsWebhook() {

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_set_webhook'
      },
      success: function( response ) {
        $( '.te-settings-webhook #webhook' ).attr( 'value', response );
      }
    });
  }

  /**
   * Setting - trigger webhook reset
   */
  $( document ).on( 'click', '.te-settings-webhook .button', function(event) {
    event.preventDefault();
    settingsWebhook();
  });

  /**
   * AJAX Save Settings
   * 
   * @param {*} thisObj 
   */
  function saveSettings( thisObj ) {

    var twilio_sid = thisObj.find( '#twilio_sid' ).val(),
        twilio_token = thisObj.find( '#twilio_token' ).val(),
        from_number = thisObj.find( '#from_number' ).val();

    showLoader();
    $( '.te-notice' ).remove();

    $.ajax({
      url: ajaxurl,
      type: 'post',
      data: {
        action: 'te_save_settings',
        twilio_sid: twilio_sid,
        twilio_token: twilio_token,
        from_number: from_number,
      },
      success: function( response ) {
        thisObj.before( '<div class="te-notice">' + response + '</div>' );
        hideLoader();
      }
    });
  }

  /**
   * Save settings trigger
   */
  $( document ).on( 'submit', 'form#settings', function(event) {
    event.preventDefault();
    saveSettings( $( this ) );    
  });

})(jQuery);