(function($){

  /**
   * selectWoo Init
   */
  $( document ).ready( function() {
    $( 'form#addContact select#lists' ).selectWoo( {
      tags: true,
      placeholder: 'Select Lists',
    } );

    $( 'form#editContact select#lists' ).selectWoo( {
      tags: true,
      placeholder: 'Select Lists',
    } );

    $( 'form#sendMessage select#list' ).selectWoo( {
      placeholder: 'Select List',
    } );
  } );

  function showLoader() {
    var top = $( window ).scrollTop();
    $( '.te-loader' ).show();
    $( '.te-loader svg' ).css( 'top', top + 'px' );
  }

  function hideLoader() {
    $( '.te-loader' ).hide();
  }

  /**
   * AJAX Add new contact
   * 
   * @param {*} thisObj 
   */
  function addContact( thisObj ) {

    var firstName = thisObj.find( '#first_name' ).val(),
        lastName  = thisObj.find( '#last_name' ).val(),
        phone     = thisObj.find( '#phone' ).val(),
        email     = thisObj.find( '#email' ).val();
        lists     = thisObj.find( '#lists' ).val();

    showLoader();
    $( '.te-notice' ).remove();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      dataType: 'json',
      data: {
        action: 'te_add_contact',
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email,
        lists: lists
      },
      success: function( response ) {
        thisObj.before( '<div class="te-notice">' + response.notice + '</div>' );
        thisObj.trigger( 'reset' );
        thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        $( '.all-contacts' ).html( response.list_contacts );
        hideLoader();
      }
    });
  }

  /**
   * Add contact trigger
   */
  $( document ).on( 'submit', 'form#addContact', function(event) {
    event.preventDefault();
    addContact( $( this ) );    
  });

  /**
   * AJAX Edit contact
   * 
   * @param {*} thisObj 
   */
  function editContact( thisObj ) {

    var id        = thisObj.find( '#id' ).val(),
        firstName = thisObj.find( '#first_name' ).val(),
        lastName  = thisObj.find( '#last_name' ).val(),
        phone     = thisObj.find( '#phone' ).val(),
        email     = thisObj.find( '#email' ).val();
        lists     = thisObj.find( '#lists' ).val();

    showLoader();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      dataType: 'json',
      data: {
        action: 'te_edit_contact',
        id: id,
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email,
        lists: lists
      },
      success: function( response ) {
        // thisObj.before( '<div class="te-notice">' + response.notice + '</div>' );
        // thisObj.trigger( 'reset' );
        // thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        $( '.contact-list' ).removeClass( 'editor-open' );
        $( '.edit-contacts' ).hide();
        $( '.all-contacts' ).html( response.list_contacts );
        hideLoader();
      }
    });
  }

  /**
   * Edit contact trigger
   */
  $( document ).on( 'submit', 'form#editContact', function(event) {
    event.preventDefault();
    editContact( $( this ) ); 
  });

  /**
   * AJAX Open edit contact form
   * 
   * @param {*} thisObj
   */
  function editContactOpen( thisObj ) {

    var contactId = thisObj.parents( '.contact-item' ).data( 'id' );
    
    showLoader();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      data: {
        action: 'te_edit_contact_form',
        id: contactId
      },
      success: function( response ) {
        $( '.contact-list' ).addClass( 'editor-open' );
        $( '.edit-contacts' ).show();
        $( '.edit-contacts' ).html( response );
        $( '.edit-contacts' ).find( 'select' ).selectWoo();
        hideLoader();
      }
    });
  }

  /**
   * Edit contact open form
   */
  $( document ).on( 'click', '.contact-item button#edit', function(event) {
    event.preventDefault();
    editContactOpen( $( this ) );
  });

  /**
   * AJAX Filter Contacts
   */
  function filterContacts() {

    var sort = $( 'form#filterContacts #sort' ).val(),
        list = $( 'form#filterContacts #list' ).val();

    showLoader();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      data: {
        action: 'te_filter_contacts',
        sort: sort,
        list: list
      },
      success: function( response ) {
        $( '.contact-list .all-contacts' ).html( response );
        hideLoader();
      }
    });
  }

  // Filter Contacts
  $( document ).on( 'change', 'form#filterContacts select', function(event) {
    event.preventDefault();
    filterContacts();
  });

  /**
   * AJAX Paginate Contacts
   */
  function paginateContacts( page ) {

    var sort = $( 'form#filterContacts #sort' ).val(),
        list = $( 'form#filterContacts #list' ).val();

    showLoader();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      data: {
        action: 'te_paginate_contacts',
        sort: sort,
        list: list,
        page: page,
      },
      success: function( response ) {
        $( '.contact-list .all-contacts' ).html( response );
        hideLoader();
      }
    });
  }

  // Paginate Contacts
  $( document ).on( 'click', '.contact-list .te-pagination a', function(event) {
    event.preventDefault();
    var page = $( this ).text();
    paginateContacts( page );
  });

  /**
   * Disable form submit
   */
  $( document ).ready( function() {
    $( 'form#importContacts input[type=submit]' ).prop( 'disabled', true );
  });

  /**
   * Upload CSV file
   */
  $( document ).on( 'change', 'form#importContacts #file_upload', function(event) {
    event.preventDefault();

    var formData = new FormData();
    var $this = $( this );
    var idField = $this.parents( '.te-form' ).find( '#file_id' );
    var submitInput = $this.parents( '.te-form' ).find( 'input[type=submit]' );

    formData.append( 'action', 'upload-attachment' );
    formData.append( 'async-upload', $this[0].files[0] );
    formData.append( 'name', $this[0].files[0].name );
    formData.append( '_wpnonce', te_frontend.nonce );

    $.ajax({
      url: te_frontend.async_upload,
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'json',
      type: 'POST',
      success: function(response) {
        idField.val( response.data.id )
        submitInput.prop( 'disabled', false );
      }
    });
  });

  /**
   * Import contacts from CSV file
   */
  $( document ).on( 'submit', 'form#importContacts', function(event) {
    event.preventDefault();

    var $this = $( this );
    var fileId = $this.find( '#file_id' ).val();

    showLoader();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      data: {
        action: 'te_import_contacts',
        file_id: fileId,
      },
      success: function( response ) {
        // $( '.contact-list .all-contacts' ).html( response );
        console.log( response );
        hideLoader();
      }
    });
  });

  /**
   * AJAX Send Message
   * 
   * @param {*} thisObj 
   */
  function sendMessage( thisObj ) {

    var message = thisObj.find( '#message' ).val(),
        list   = thisObj.find( '#list' ).val();

    showLoader();
    $( '.te-notice' ).remove();

    $.ajax({
      url: te_frontend.admin_ajax,
      type: 'post',
      data: {
        action: 'te_send_message',
        message: message,
        list: list
      },
      success: function( response ) {
        thisObj.before( '<div class="te-notice">' + response + '</div>' );
        thisObj.trigger( 'reset' );
        thisObj.find( 'select' ).selectWoo( 'val', 'All' );
        hideLoader();
      }
    });
  }

  /**
   * Send message trigger
   */
  $( document ).on( 'submit', 'form#sendMessage', function(event) {
    event.preventDefault();
    sendMessage( $( this ) );    
  });





  // Edit Contact - Old
  $( document ).on( 'submit', '.te-edit-contact', function(event) {
    event.preventDefault();

    $this = $( this );

    var postId = $this.data( 'id' );
    var firstName = $this.find( '#first-name' ).val();
    var lastName = $this.find( '#last-name' ).val();
    var phoneNumber = $this.find( '#phone-number' ).val();
    var email = $this.find( '#email' ).val();
    var lists = $this.find( '#lists' ).val();

    // console.log( postId );

    $.ajax({
      url: ajax.ajaxurl,
      type: 'post',
      data: {
        action: 'te_edit_contact',
        post_id: postId,
        first_name: firstName,
        last_name: lastName,
        phone_number: phoneNumber,
        email: email,
        lists: lists
      },
      success: function( response ) {
        // $( '.te-form form' ).remove();
        Materialize.toast( 'Contact Edited!', 3000, 'rounded');
        $( '.modal' ).modal( 'close' );
        $( '.te-edit-contact' ).append( response );
        // console.log(response);
        // $( 'select' ).material_select();
      }
    });

  });


  $( document ).on( 'click', '.list-contacts button', function(event) {
    event.preventDefault();

    var postId = $( this ).data( 'id' );
    var modalId = '#modal-' + postId;
    
    $.ajax({
      url: ajax.ajaxurl,
      type: 'post',
      data: {
        action: 'te_edit_modal',
        post_id: postId
      },
      success: function( response ) {
        $( 'body' ).append( response );
        $( modalId ).modal( 'open' );
        $( 'select' ).material_select();
      }
    });

  });


  $( document ).on( 'click', '.upload-contacts .upload-contacts-submit', function(event) {
    event.preventDefault();

    var csvFile = $( '.upload-contacts #csvFile' ).prop('files')[0];
    
    $.ajax({
      url: ajax.ajaxurl,
      type: 'post',
      dataType: 'json',
      processData: false,
      data: {
        action: 'te_upload_contacts',
        csv_file: csvFile
      },
      success: function( response ) {
        console.log( response );
        $( '.page-header' ).append( response );
      }
    });

  });


  // Create List
  $( document ).on( 'submit', '.te-create-lists', function(event) {
    event.preventDefault();

    var listName = $( '.te-create-lists #list-name' ).val();

    $.ajax({
      url: ajax.ajaxurl,
      type: 'post',
      data: {
        action: 'te_create_list',
        list_name: listName
      },
      success: function( response ) {
        $( '.te-form form' ).remove();
        Materialize.toast( 'List Added!', 3000, 'rounded');
        $( '.te-form' ).append( response );
        $( 'select' ).material_select();

        console.log(response);
      }
    });

  });



  // $(document).on('click', '.save-support', function (e) {

  //   var supporttitle = $('.support-title').val();

  //   var querytype = $('.support-query').val();
  //   var file_data = $('#sortpicture').prop('files')[0];

  //   console.log(file_data);

  //   var form_data = new FormData();
  //   if (supporttitle == '') {
  //       $('.support-title').css({"border": "1px solid red"})
  //       return false;
  //   } else {
  //       $('.support-title').css({"border": "1px solid #e3ecf0"})
  //   }

  //   form_data.append('file', file_data);
  //   form_data.append('action', 'md_support_save');
  //   form_data.append('supporttitle', supporttitle);

  //   $.ajax({
  //       url: ajax.ajaxurl,
  //       type: 'post',
  //       contentType: false,
  //       processData: false,
  //       data: form_data,
  //       success: function (response) {
  //           $('.Success-div').html("Form Submit Successfully")
  //       },
  //       error: function (response) {
  //        console.log('error');
  //       }
  //   });
  // });

})(jQuery);